<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

class ReadXmlCommand extends Command
{
	function configure()
	{
	    $this
	        ->setName('xml:read')
	        ->setDescription('Importa os dados de um arquivo em XML')
	    ;
    }
    function execute(InputInterface $input, OutputInterface $output)
    {
    	$xmlPath = 'data/sitemap.xml';

		$xml = simplexml_load_file($xmlPath);

		$count = 0;

		foreach ($xml->url as $url)
		{
			$count++;
			print "
		---------------------------------------------------------------

			ID: $count
			URL: $url->loc
			Última data de modificação: $url->lastmod
			Frequência de atualização: $url->changefreq
			Prioridade: $url->priority
			";
		}
		print "
		---------------------------------------------------------------
";
    }
}