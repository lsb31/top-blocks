<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

class ConvertCommand extends Command
{
	function configure()
	{
	    $this
	        ->setName('xml:convert')
	        ->setDescription('Converte um XML para YAML')
	    ;
    }
    function execute(InputInterface $input, OutputInterface $output)
    {
    	$xmlInputPath = 'data/sitemap.xml';

    	$ymlOutputPath = 'data/resultado.yml';

		$xml = simplexml_load_file($xmlInputPath);

		$array = [];

		$id = 0;

		foreach ($xml as $key => $value) {
			$array[] = [
				'url' => (string)$value->loc,
				'last_modification' => (string)$value->lastmod,
				'change_frequency' => (string)$value->changefreq,
				'priority' => (string)$value->priority,
				'ID' => $id
			];
			$id++;
		}

		$yaml = Yaml::dump($array);

		file_put_contents($ymlOutputPath, $yaml);
    }
}